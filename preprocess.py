import os
import warnings

import pandas as pd
import numpy as np
from sklearn.preprocessing import StandardScaler
from tqdm import tqdm

warnings.filterwarnings("ignore")

INPUT_DATA_PATH = os.path.abspath(
    os.path.join(os.path.dirname(__file__), "data/train_files/stock_prices.csv")
)
OUTPUT_PATH = os.path.abspath(
    os.path.join(os.path.dirname(__file__), "out/stock_prices_std_outlier.csv")
)


def main():
    prices = pd.read_csv(INPUT_DATA_PATH)

    df = pd.DataFrame(columns=prices.columns)

    for id in tqdm(prices.SecuritiesCode.unique()):
        std_data = prices[prices.SecuritiesCode == id]
        ss = StandardScaler()
        cols = ["Open", "High", "Low", "Close", "Volume"]
        std_data.loc[:, cols] = ss.fit_transform(std_data.loc[:, cols])
        std_data.loc[:, cols].apply(calculateOutlier, axis=0)
        price_std = pd.concat([df, std_data])
        df = price_std

    df = df.sort_values(["Date", "SecuritiesCode"])

    df.to_csv(OUTPUT_PATH)


def calculateOutlier(column):
    mu = column.mean()
    std = column.std()
    outlier = column[np.abs(column - mu) > 3 * std]
    if pd.isnull(outlier.any()):
        return column
    else:
        column.clip(lower=mu - 3 * std, upper=mu + 3 * std)
    return


def some_work():
    print("Doing work...")


if __name__ == "__main__":
    main()
